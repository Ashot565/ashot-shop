$( document ).ready(function() {
    $('.carousel').carousel();


    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:true
            },
            1000:{
                items:3,
                nav:true,
                loop:false
            }
        }
    });




    var $grid = $('.grid').isotope({

        // set itemSelector so .grid-sizer is not used in layout
        itemSelector: '.grid-item',
        percentPosition: true,
        stagger: 70,
        masonry: {
            // use element for option
            columnWidth: '.grid-item'
        }
    });
// bind filter button click
    $filters = $('.portfolio-btn').on( 'click', 'button', function() {
        var $this = $( this );
        var filterValue;
        if ( $this.is('.is-checked') ) {
            // uncheck
            filterValue = '*';
        } else {
            filterValue = $this.attr('data-filter');
            $filters.find('.is-checked').removeClass('is-checked');
        }
        $this.toggleClass('is-checked');

        // use filterFn if matches value
        var filterValue = $(this).attr('data-filter');
            $grid.isotope({ filter: filterValue });
    });




    $(window).scroll(function (x) {
        x.preventDefault();

        if ($(document).scrollTop() > 300){
            $('.scroll').addClass('up');

        }
        else{
            $('.scroll').removeClass('up');
        }

});

    $('#scrollUp').on('click', function () {

        $('html').animate({
            scrollTop: 0
        }, 1200);
    })



});

$("#ex2").slider({});











