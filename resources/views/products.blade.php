@extends('layouts.front')

@section('title','Products')

@section('sidebar')
    @parent
@endsection

@section('content')
    <main>

        <div class="container">
            <div class="row prod">
                <section class="col-md-9 col-sm-12 main-content " >

                    <div class="main1">
                        <h3><span>FEATURES ITEMS</span></h3>

                        @isset($products)

                            @foreach($products as $product)
                                {{--@php $img = json_decode($product->posters); @endphp--}}
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="img-block">
                                        <a href="{{route('product', $product->id)}}"><img src="{{asset('images/' . json_decode($product->posters)[0])}}" alt=""></a>
                                        <div class="text-center">
                                            <h2>${{$product->price}}</h2>
                                            <a href="{{route('product', $product->id)}}"><p>{{$product->title}}</p></a>
                                            <a href="" class="btn btn-default"><i class="fa fa-shopping-cart" style="margin-right: 10px"></i>Add To Cart</a>
                                        </div>
                                    </div>
                                    <div class=" add-block">
                                        <ul>
                                            <li class="col-xs-6"><a href=""><i class="fa fa-plus-square" ></i> Add to wishlist</a></li>
                                            <li class="col-xs-6 text-right"><a href=""><i class="fa fa-plus-square" ></i> Add to compare</a></li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        @endisset
                        {{$products->links()}}
                    </div>

                </section>
            </div>
        </div>

    </main>
@endsection