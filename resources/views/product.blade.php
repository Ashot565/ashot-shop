@extends('layouts.front')

@section('title','Product')

@section('sidebar')
@parent
@endsection

@section('content')
<main>

    <div class="container">
        <div class="row">
            <section class="col-md-9 col-sm-12 main-content " >
                @isset($product)
                        @php $img = json_decode($product->posters) @endphp
                <div class="product-detalis">
                    <div class="col-sm-5">
                        <div class="col-sm-12">
                            <div class="img-block" style="width: 100%;"><img src="{{asset('images/'.$img[0])}}" alt="" style="width: 100%;border: 1px solid #F7F7F0;height: 400px;"></div>
                        </div>
                        <div class="col-sm-12 main3">
                            <div class="owl-carousel product-carousel main1">
                                @foreach($img as $key => $image)
                                    @if($key !=0)
                                        <div class="">
                                            <div class="img-block">
                                                <img src="{{asset('images/'.$image)}}" alt="">
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="product-detalis-info">
                            <h2>{{$product->title}}</h2>
                            <p>{{$product->web_id}}</p>
                            <span>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>

                            </span>
                            <span>${{$product->price}}</span>
                            <form action="{{route('add-to-cart')}}" method="post">
                                @csrf
                                <input type="hidden" name="title" value="{{$product->title}}">
                                <input type="hidden" name="poster" value="{{json_decode($product->posters)[0]}}">
                                <input type="hidden" name="web_id" value="{{$product->web_id}}">
                                <input type="hidden" name="price" value="{{$product->price}}">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id ?? null}}">
                                @auth
                                <div>Quantity:<input type="text" name="quantity" value="1" style="width: 15%;    padding: 3px 0px;"></div>
                                <button type="submit" class="btn btn-default" >
                                    <i class="fa fa-shopping-cart" style="margin-right: 10px"></i>Add To Cart
                                </button>
                                    @endauth
                            </form>
                            <div>

                                <p><b>Availability:</b>
                                    @if($product->availability == 1)
                                        In Stock
                                        @else
                                        No
                                    @endif</p>
                                <p><b>Condition:</b>
                                    @if($product->condition == 1)
                                        New
                                    @else
                                        Old
                                    @endif
                                </p>
                                <p><b>Brand:</b> E-SHOPPER</p>
                                <a href=""><img src="images/blogs/socials.png" alt=""></a>
                            </div>

                        </div>
                    </div>
                </div>

                @endisset
                <div class="col-sm-12 product-reviews">
                    <h3>REVIEWS</h3>

                    <details>
                        <summary>Comments</summary>
                        @if(isset($comments) && count($comments)>0)

                            @foreach($comments as $comment)
                                <div>
                                    <ul>
                                        <li><i class="fa fa-user"></i>{{$comment->name}}</li>
                                        <li><i class="fa fa-clock-o"></i>1:33 pm</li>
                                        <li><i class="fa fa-calendar"></i>DEC 5, 2013</li>
                                    </ul>
                                    <p>{{$comment->text}}</p>
                                </div>
                            @endforeach
                        @else
                            <p id="noCom">No comments yet</p>
                        @endif
                    </details>


                    <form action="" id="resetForm">
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <h4>Write Your Reviews</h4>
                        <div class="col-sm-6"><input type="name" class="form-control" name="name" placeholder="Your Name"></div>
                        <div class="col-sm-6"><input type="email" class="form-control" name="email" placeholder="Your Email"></div>
                        <div class="col-sm-12"><textarea name="text" class="form-control" id="" cols="30" rows="10"></textarea></div>
                        <div class="col-sm-12" style="text-align: right;margin-top: 15px"><input type="reset" id="comment" class="btn" value="send"></div>
                    </form>
                </div>


            </section>
            <div class="col-sm-12 main3 product-detalis-slider">
                <h3><span>RECOMMENDED ITEMS</span></h3>

                <div id="id_carousel2" class="carousel slide">
                    <!-- Slides -->
                    <div class="carousel-inner">

                        @foreach ($recomentedItems->chunk(3) as $key => $chunk)
                            @if($key == 0)
                                <div class="item active">
                                    @else
                                        <div class="item">
                                            @endif
                                            <div class="row">
                                                @foreach ($chunk as $recomentedItem)
                                                    @php $img = json_decode($recomentedItem->posters);  @endphp
                                                    <div class="col-xs-12 col-sm-4"->
                                                        <div class="text-center carousel_only_product">
                                                            <div class="carousel_product_image ">
                                                                <a href="{{route('product', $recomentedItem['id'])}}"><img src="{{asset('images/' . $img[0])}}" alt="product1"></a>
                                                            </div>
                                                            <p>{{$recomentedItem->price}}$</p>
                                                            <a href="{{route('product', $recomentedItem['id'])}}">{{$recomentedItem->title}}</a>
                                                            <form action="{{route('product',$recomentedItem['id'])}}" method="get">
                                                                <button type="submit" class="btn btn-default">View</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endforeach
                                </div>
                                <!-- Controls -->
                                <a class="" href="#id_carousel2" data-slide="prev">
                                    <i class="fa fa-angle-left left_arrow_carousel2"></i>
                                </a>
                                <a class="" href="#id_carousel2" data-slide="next">
                                    <i class="fa fa-angle-right right_arrow_carousel2"></i>
                                </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

</main>
@endsection

@section('script')
    <script>
        $(document).on('click','#comment', function () {

            var name = $('input[name = name]').val();
            var email = $('input[name = email]').val();
            var text = $('textarea[name = text]').val();
            var product_id = $('input[name = id]').val();

            var commentObj = {

                name:name,
                email:email,
                text:text,
                product_id:product_id,
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:"{{route('comment')}}" ,
                type: "POST",
                data: commentObj,
                success: function (resp) {
                    $('#noCom').text('');
                    $('<div><ul><li><i class="fa fa-user"></i>'+ name +'</li><li><i class="fa fa-clock-o"></i>1:33 pm</li><li><i class="fa fa-calendar"></i>DEC 5, 2013</li></ul><p>'+text+'</div>').insertAfter('summary');
                    console.log('dfdfedfe');
                }
            });
        })
    </script>
@endsection