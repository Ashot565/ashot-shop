@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">PRODUCT</h4>
                            <h3 class="card-description">
                                PRODUCT TITLE
                            </h3>
                            <form class="forms-sample " action="{{route('products.update',$product->id)}} " method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <p>Product Name</p>
                                    <input type="text" class="form-control" name="title" value="{{$product->title}}" placeholder="add new Product">
                                </div>
                                <div class="form-group">
                                    <p>Availability</p>
                                    <select class="custom-select" name="availability"  >
                                        @if($product->availability == 1)
                                        <option selected  value="1">Yes</option>
                                        <option value="0">No</option>
                                            @else
                                            <option  value="1">Yes</option>
                                            <option selected value="0">No</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p>Condition</p>
                                    <select class="custom-select"  name="condition"  >
                                        @if($product->availability == 1)
                                            <option selected value="1">New</option>
                                            <option value="0">Old</option>
                                        @else
                                            <option value="1">New</option>
                                            <option selected value="0">Old</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p>Quantity</p>
                                    <input type="number" class="form-control" name="quantity" placeholder="Add quantity" value="{{$product->quantity}}">
                                </div>
                                <div class="form-group">
                                    <p>Price</p>
                                    <input type="number" class="form-control" name="price" placeholder="add price" value="{{$product->price}}">
                                </div>
                                <div class="form-group">
                                    <p>Web_id</p>
                                    <input type="text" class="form-control" name="web_id" placeholder="add web_id" value="{{$product->web_id}}">
                                </div>
                                <div class="form-group">
                                    <p>Posters</p>
                                    <input type="file" class="form-control" name="posters[]" placeholder="add posters" multiple >
                                </div>
                                <div class="form-group">
                                    <p>Category</p>
                                    <select class="custom-select"  name="category_id"  >
                                        @foreach($categories as $category)
                                            @if($product->category->id == $category->id )
                                            <option value="{{$category->id}}" selected>{{$category->title}}</option>
                                                @else
                                                <option value="{{$category->id}}">{{$category->title}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection