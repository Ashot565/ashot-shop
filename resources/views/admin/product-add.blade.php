@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">PRODUCT</h4>
                            <h3 class="card-description">
                                PRODUCT TITLE
                            </h3>
                            <form class="forms-sample " action="{{route('products.store')}} " method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <p>Product Name</p>
                                    <input type="text" class="form-control" name="title" placeholder="add new Product">
                                </div>
                                <div class="form-group">
                                    <p>Availability</p>
                                    <select class="custom-select" name="availability" >

                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p>Condition</p>
                                    <select class="custom-select"  name="condition" id="" >
                                        <option value="1">New</option>
                                        <option value="0">Old</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p>Quantity</p>
                                    <input type="number" class="form-control" name="quantity" placeholder="Add quantity">
                                </div>
                                <div class="form-group">
                                    <p>Price</p>
                                    <input type="number" class="form-control" name="price" placeholder="add price">
                                </div>
                                <div class="form-group">
                                    <p>Web_id</p>
                                    <input type="text" class="form-control" name="web_id" placeholder="add web_id">
                                </div>
                                <div class="form-group">
                                    <p>Posters</p>
                                    <input type="file" class="form-control" name="posters[]" placeholder="add posters" multiple >
                                </div>
                                <div class="form-group">
                                    <p>Category</p>
                                    <select class="custom-select"  name="category_id" id="" >
                                        @foreach($categories as $category)
                                           <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection