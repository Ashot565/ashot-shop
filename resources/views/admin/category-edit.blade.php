@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">CATEGORY</h4>
                            <p class="card-description">
                                CATEGORY TITLE
                            </p>
                            <form class="forms-sample " action="{{route('category.update',$category->id )}} " method="post">
                                @csrf
                                @method ('put')
                                <div class="form-group">
                                    <input type="text" class="form-control" name="title" value="{{$category->title}}" placeholder="Enter title">
                                </div>
                                <button type="submit" class="btn btn-success mr-2">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection