@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Product List</h4>
                    <p class="card-description">
                        <a href="{{route('products.create')}}" class="btn btn-success">Add Product</a>
                    </p>
                    <table class="table table-dark ">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Availability
                            </th>
                            <th>
                                Condition
                            </th>
                            <th>
                                Quantity
                            </th>
                            <th>
                                Price
                            </th>
                            <th>
                                Web_id
                            </th>
                            <th>
                                Posters
                            </th>
                            <th>
                                Category
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($products)&& is_object($products))
                            @foreach($products as $key=> $product)
                                <tr>
                                    <td>
                                        {{++$key}}
                                    </td>
                                    <td>
                                        {{$product->title}}
                                    </td>
                                    <td>

                                        @if($product->availability == 1)
                                            Yes
                                        @else
                                            No
                                        @endif

                                    </td>
                                    <td>

                                        @if($product->condition == 1)
                                            New
                                        @else
                                            Old
                                        @endif
                                    </td>
                                    <td>
                                        {{$product->quantity}}
                                    </td>
                                    <td>
                                        {{$product->price}}
                                    </td>
                                    <td>
                                        {{$product->web_id}}
                                    </td>
                                    <td>

                                        @foreach(json_decode($product->posters) as $name)
                                            <img src="{{asset('/images/'.$name)}}" alt="Img" >
                                        @endforeach
                                    </td>
                                    <td>
                                        {{$product->category->title}}
                                    </td>
                                    <td>
                                        <a href="{{route('products.edit',$product->id)}}" class="btn btn-primary">Edit</a>
                                        <form action="{{route('products.destroy', $product->id )}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Delete</button>


                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
@endsection