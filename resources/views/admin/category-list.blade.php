@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Category List</h4>
                    <p class="card-description">
                        <a href="{{route('category.create')}}" class="btn btn-success">Add Category list</a>
                    </p>
                    <table class="table table-dark">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Created At
                            </th>
                            <th>
                                Updated At
                            </th>
                            <th>
                                Sount
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(isset($categories)&& is_object($categories))
                            @foreach($categories as $key=> $category)
                        <tr>
                            <td>
                                {{++$key}}
                            </td>
                            <td>
                                {{$category->title}}
                            </td>
                            <td>
                                {{$category->created_at}}
                            </td>
                            <td>
                                {{$category->updated_at}}
                            </td>
                            <td>
                                {{$category->product->count()}}
                            </td>
                            <td>
                                <a href="{{route('category.edit',$category->id)}}" class="btn btn-primary">Edit</a>
                                <form action="{{route('category.destroy', $category->id )}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>


                                </form>
                            </td>
                        </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div >{{$categories->links()}}</div>
                </div>

            </div>
    </div>
@endsection