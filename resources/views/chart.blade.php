@extends('layouts.front')

@section('title', 'Chart')

@section('sidebar')

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="u-info-purch-block">
                <h4 class="u-info">Personal information</h4>
                <div class="media user-avatar-media">
                    <div class="media-left user-avatar-media-left">
                        <img id="u_avatar" style="width: 150px" src="{{ (Auth::user()->avatar) ? asset('images') . '/' . Auth::user()->avatar : asset('images/avatar.jpg') }}" class="media-object">
                        <label class="user-avatar-label" for="user-avatar">Change avatar <i class="fa fa-picture-o" aria-hidden="true"></i></label>
                        <form id="avatar-form" action="#" method="post" enctype="multipart/form-data" class="hidden">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <input type="file" name="user_avatar" id="user-avatar">
                        </form>
                    </div>
                    <div class="media-body">
                        <ul class="list-group media-list">
                            <li class="list-group-item"><b>Name:</b> {{ Auth::user()->name }}</li>
                            <li class="list-group-item"><b>E-mail:</b> {{ Auth::user()->email }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="u-info-purch-block">
                <h4 class="u-purch">User Purchases</h4>
                <div class="table-responsive cart_table_div">
                    <table class="table ">
                        <thead>
                        <tr class="cart_head">
                            <th>Item</th>
                            <th></th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--<tr>--}}
                        {{--<td>--}}
                        {{--<div class="cart_image">--}}
                        {{--<a href="#"><img src="images/hp_g1610t.jpg" alt="Product 2"></a>--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--<td class="cart_description">--}}
                        {{--<a href="#">HP G1610T MicroServer</a>--}}
                        {{--<p>Web ID: 1089684</p>--}}
                        {{--</td>--}}
                        {{--<td class="cart_price">--}}
                        {{--<p class="cart_unit_price">$1864</p>--}}
                        {{--</td>--}}
                        {{--<td class="cart_quantity">--}}
                        {{--<button type="button" class="cart_quantity_minus">-</button>--}}
                        {{--<input class="form-control cart_quantity_input" type="text" value="1" autocomplete="off">--}}
                        {{--<button type="button" class="cart_quantity_plus">+</button>--}}
                        {{--</td>--}}
                        {{--<td class="cart_total">--}}
                        {{--<p class="cart_total_price"></p>--}}
                        {{--<button class="btn btn-default">BUY</button>--}}
                        {{--</td>--}}
                        {{--<td class="cart_remove">--}}
                        {{--<button class="btn btn-default cart_remove_button" type="button">--}}
                        {{--<i class="fa fa-times" aria-hidden="true"></i>--}}
                        {{--</button>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    @if(isset($products) && is_object($products))
        <div class="table-responsive cart_table_div">
            <table class="table ">
                <thead>
                <tr class="cart_head">
                    <th>Item</th>
                    <th></th>
                    <th style="width: 17%">Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td class="cart_image">
                            <a href="#"><img src="{{ asset('images') . '/' . $product->poster }}" alt="{{ $product->poster }}" style="width: 100px; height: 100px"></a>
                        </td>
                        <td class="cart_description">
                            <a href="#">{{ $product->title }}</a>
                            <p>Web ID: {{ $product->web_id }}</p>
                        </td>
                        <td class="cart_price">
                            <p class="cart_unit_price">${{ $product->price }}</p>
                        </td>
                        <td class="cart_quantity" style="display: -webkit-inline-box;    padding-top: 25px;">
                                <button class="btn btn-default cart_quantity_minus">-</button>
                                <input type="number" class="form-control cart_quantity_input" value="{{ $product->quantity }}" autocomplete="off" style="width: 25%">
                                <button class="btn btn-default cart_quantity_plus">+</button>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">${{ $product->total_price }}</p>
                            <form action="" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" id="total_amount" name="total" value="{{ $product->total_price }}">
                                <input type="hidden" name="web_id" value="{{$product->web_id}}">
                                <input type="hidden" name="quantity" value="{{$product->quantity}}">
                                <button class="btn btn-primary">BUY</button>
                            </form>
                        </td>
                        <td class="cart_remove">
                            <form action="#" method="post" class="product_hide_form">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <button class="btn btn-warning cart_remove_button" type="submit" style="background: red">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection