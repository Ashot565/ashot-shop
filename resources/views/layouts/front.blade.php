<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
    {{--<link rel="stylesheet" href="{{asset('css/slider.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="shortcoon icon" href="{{asset('images/' . 'favicon.png')}}">
    @yield('styles')
</head>
<body>

<header>
    <div class="header-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 tel">
                    <a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span>+374 010 23 45 77</span></a>
                    <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i><span>petrosgrt@mail.ru</span></a>
                </div>
                <div class="col-sm-6 follow text-right">
                    <ul>
                        <a href="route" target="_blank">
                            <li><i class="fa fa-facebook"></i></li>
                        </a>
                        <a href="https://twitter.com/" target="_blank">
                            <li><i class="fa fa-twitter"></i></li>
                        </a>
                        <a href="https://www.instagram.com/" target="_blank">
                            <li><i class="fa fa-linkedin"></i></li>
                        </a>
                        <a href="https://plus.google.com/" target="_blank">
                            <li><i class="fa fa-google-plus"></i></li>
                        </a>
                        <a href="https://telegram.org/" target="_blank">
                            <li><i class="fa fa-telegram"></i></li>
                        </a>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-logo">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 ">
                    <div class="img-block"><a href="{{route('index')}}"><img src="{{asset('images/' . 'favicon.png')}}" class="img-responsive"
                                                                     alt="Logo"></a></div>
                </div>
                <div class="col-sm-6 col-xs-12 text-right">
                    <ul>

                        {{--<a href="">--}}
                            {{--<li><i class="fa fa-star"></i><span>Wishlist</span></li>--}}
                        {{--</a>--}}
                        {{--<a href="checkout.html">--}}
                            {{--<li><i class="fa fa fa-crosshairs"></i><span>Checkout</span></li>--}}
                        {{--</a>--}}
                        @guest

                        <a href="" data-toggle="modal" data-target=".login-register-form">
                            <li><i class="fa fa-lock"></i><span>Acount</span></li>
                        </a>
                        @else
                            <a href="{{route('chart')}}" >
                                <li><i/ class="fa fa-user"></i><span>{{ Auth::user()->name }}</span></li>
                            </a>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    Logout
                                </a>
                                {{--<!-- Left Side Of Navbar -->--}}
                                {{--<ul class="nav navbar-nav">--}}
                                {{--&nbsp;<li><a href="{{ route('cat-list') }}">Категории</a></li>--}}
                                {{--<li><a href="{{ route('sub-category.index') }}">Подкатегории</a></li>--}}
                                {{--<li><a href="{{ route('brands.index') }}">Бренды</a></li>--}}
                                {{--<li><a href="{{ route('product.index') }}">Продукты</a></li>--}}
                                {{--</ul>--}}
                                {{--<!-- Right Side Of Navbar -->--}}
                                <ul class="shop_page dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <div class="row">

                <nav class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{route('index')}}">
                                <div class="img-block"><img src="{{asset('images/' . 'logo.png')}}" class="img-responsive" alt="Logo">
                                </div>
                            </a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{route('index')}}">Home</a></li>
                                <li><a href="{{route('products')}}">Products</a></li>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Contact</a></li>
                            </ul>
                            <form action="{{route('search')}}" class="navbar-form navbar-right" role="search" method="get">
                                {{--@csrf--}}
                                <div class="form-group">
                                    <input name="search" type="text" class="form-control" placeholder="Search">
                                </div>
                                <button type="submit" class="btn btn-default fa fa-search"></button>
                            </form>

                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>

            </div>
        </div>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            @yield('carousel')
            @section('sidebar')
                <section class="col-md-3 col-sm-12 left-sidebar">

                    <div class="left1">
                        <h3><span>CATEGORY</span></h3>

                        <ul>
                            @isset($categoryList)

                                @foreach($categoryList as $category)
                                    <a href="{{route('category', $category->id)}}">
                                        <li>{{$category->title}}</li>
                                    </a>
                                @endforeach
                            @endisset
                        </ul>
                    </div>

                    <div class="left2">
                        <h3><span>BRANDS</span></h3>

                        <ul>
                            <a href="#.">
                                <li>ACNE <span>(50)</span></li>
                            </a>
                            <a href="#.">
                                <li>GRÜNE ERDE <span>(56)</span></li>
                            </a>
                            <a href="#.">
                                <li>ALBIRO <span>(27)</span></li>
                            </a>
                            <a href="#.">
                                <li>RONHILL <span>(32)</span></li>
                            </a>
                            <a href="#.">
                                <li>ODDMOLLY <span>(5)</span></li>
                            </a>
                            <a href="#.">
                                <li>BOUDESTIJN <span>(9)</span></li>
                            </a>
                            <a href="#.">
                                <li>RÖSCH CREATIVE <span>(4)</span></li>
                            </a>
                        </ul>
                    </div>

                    {{--<div class="left3">--}}
                        {{--<h3><span>PRICE RANGE</span></h3>--}}

                        {{--<ul>--}}
                            {{--<b>€ 10</b>--}}
                            {{--<input id="ex2" type="text" class="span2" value="" data-slider-min="10"--}}
                                   {{--data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/>--}}
                            {{--<b>€ 1000</b>--}}
                        {{--</ul>--}}

                        {{--@php  $uri = explode('/', $_SERVER['REQUEST_URI']);@endphp--}}
                                {{--@if ($uri[1] == 'category')--}}
                                    {{--@include('layouts.partials.range')--}}
                                {{--@endif--}}




                    {{--</div>--}}

                    <div class="left4">
                        <ul><img src="{{asset('images/' . 'shipping.jpg')}}" alt=""></ul>
                    </div>
                </section>
            @show
            @yield('content')
        </div>
    </div>
</main>


<footer>
    <div class="container">
        <div class="row">
            <div class="video-block col-sm-12 col-xs-12">
                <div class="col-sm-3 col-xs-12">
                    <div class="img-block"><img src="{{asset('images/' . 'favicon.png')}}" class="img-responsive" alt=""
                                                style="    width: 135px;margin-top: -22px;"></div>
                    <p class="info">Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempo</p>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="img-block">
                            <img src="{{asset('images' . '/products/'. 'recommend1.jpg')}}" alt="">
                            <p class="text-center">Circle of Hands <br>
                                24 DEC 2014</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="img-block">
                            <img src="{{asset('images' . '/products/'. 'recommend1.jpg')}}" alt="">
                            <p class="text-center">Circle of Hands <br>
                                24 DEC 2014</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="img-block">
                            <img src="{{asset('images' . '/products/'. 'recommend1.jpg')}}" alt="">
                            <p class="text-center">Circle of Hands <br>
                                24 DEC 2014</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="img-block">
                            <img src="{{asset('images' . '/products/'. 'recommend1.jpg')}}" alt="">
                            <p class="text-center">Circle of Hands <br>
                                24 DEC 2014</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12 map-foto">
                    <div class="img-block">
                        <img src="{{asset('images/' . 'map.png')}}" class="img-responsive" alt="">
                        <p class="text-center">505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12" style="padding-top: 40px; padding-bottom: 20px">
                <div class="col-sm-9">
                    <div class="col-sm-3">
                        <ul>
                            <h5>SERVICE</h5>
                            <li><a href="">Online Help</a></li>
                            <li><a href="">Contact Us</a></li>
                            <li><a href="">Order Status</a></li>
                            <li><a href="">Change Location</a></li>
                            <li><a href="">FAQ’s</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul>
                            <h5>QUOCK SHOP</h5>
                            <li><a href="">T-Shirt</a></li>
                            <li><a href="">Mens</a></li>
                            <li><a href="">Womens</a></li>
                            <li><a href="">Gift Cards</a></li>
                            <li><a href="">Shoes</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul>
                            <h5>POLICIES</h5>
                            <li><a href="">Terms of Use</a></li>
                            <li><a href="">Privecy Policy</a></li>
                            <li><a href="">Refund Policy</a></li>
                            <li><a href="">Billing System</a></li>
                            <li><a href="">Ticket System</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul>
                            <h5>ABOUT SHOPPER</h5>
                            <li><a href="">Company Information</a></li>
                            <li><a href="">Careers</a></li>
                            <li><a href="">Store Location</a></li>
                            <li><a href="">Affillate Program</a></li>
                            <li><a href="">Copyright</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <h5>ABOUT SHOPPER</h5>
                    <form action="" method="post">
                        <input type="email" class="send-email">
                        <button><i class="fa fa-arrow-circle-o-right"></i></button>
                    </form>
                    <p class="info-updates">Get the most recent updates from
                        our site and be updated your self...</p>
                </div>
            </div>

        </div>
    </div>

    <div>
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © E-SHOPPER Inc. All rights reserved.</p>
                <p class="pull-right">Designed by <span>PHP-GROUP-4</span></p>
            </div>
        </div>
    </div>
</footer>


<div class="loginModal">
    <style>
        .modal-header {
            padding: 0;
        }
        .modal-header .close {
            padding: 10px 15px;
        }
        .modal-header ul {
            border: none;
        }
        .modal-header ul li {
            margin: 0;
        }
        .modal-header ul li a {
            border: none;
            border-radius: 0;
        }
        .modal-header ul li.active a {
            color: #e12f27;
        }
        .modal-header ul li a:hover {
            border: none;
        }
        .modal-header ul li a span {
            margin-left: 10px;
        }
        .modal-body .form-group {
            margin-bottom: 10px;
        }

    </style>
    {{--<a class="btn btn-primary" href="#" data-toggle="modal" data-target=".login-register-form">--}}
        {{--Login - Registration Modal--}}
    {{--</a>--}}

    <!-- Login / Register Modal-->
    <div class="modal fade login-register-form" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#login-form"> Login <span class="glyphicon glyphicon-user"></span></a></li>
                        <li><a data-toggle="tab" href="#registration-form"> Register <span class="glyphicon glyphicon-pencil"></span></a></li>
                    </ul>
                </div>
                <div class="modal-body">
                    <div class="tab-content">
                        <div id="login-form" class="tab-pane fade in active">
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Enter email" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="pwd" placeholder="Enter password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me</label>
                                </div>
                                <button type="submit" class="btn btn-default">Login</button>
                            </form>
                        </div>
                        <div id="registration-form" class="tab-pane fade">
                            <form action="{{ route('register') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Your Name:</label>
                                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Enter your name" name="name" value="{{ old('name') }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="newemail">Email:</label>
                                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="newemail" placeholder="Enter new email" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="newpwd">Password:</label>
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="newpwd" placeholder="New password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group ">
                                    <label for="password-confirm" >Confirm Password</label>
                                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirm password" name="password_confirmation" required>
                                </div>
                                <button type="submit" class="btn btn-default">Register</button>
                            </form>
                        </div>

                    </div>
                </div>
                <!--                                    <div class="modal-footer">-->
                <!--                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                <!--                                    </div>-->
            </div>
        </div>
    </div>
</div>

<a id="scrollUp" class="scroll" href="#top"><i class="fa fa-angle-up"></i></a>
{{--<script src="{{asset('js/JQ.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
{{--<script src="{{asset('js/bootstrap-slider.js')}}"></script>--}}
<script src="{{asset('js/image%20load.js')}}"></script>
<script src="{{asset('js/isotope.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
@yield('script')
</body>
</html>