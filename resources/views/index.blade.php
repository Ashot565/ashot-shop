@extends('layouts.front')

@section('title', 'Eshop Homepage')

@section('carousel')
    <section class="main-slider">
        @isset($sliders)
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">

                    @foreach($sliders as $key => $slider)
                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"
                            class="@if($key==0) active @endif"></li>
                    @endforeach

                </ol>

                <!-- Wrapper for slides -->
                <div class=" slider carousel-inner">
                    @foreach($sliders as $key => $slider)
                        <div class=" item text-justify @if($key==0) active @endif">
                            <div class="">
                                <img src="{{asset('images/'.$slider->path)}}" class="img-responsive" alt="">
                            </div>

                        </div>
                    @endforeach

                </div>


                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        @endisset
    </section>
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    <section class="col-md-9 col-sm-12 main-content ">

        <div class="main1" style="height: 1000px;">
            <h3><span>FEATURES ITEMS</span></h3>

            <!--<div>-->
            @isset($featuresList)
                @foreach($featuresList as $feature)
                    @php $img = json_decode($feature->posters) @endphp
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="img-block">
                            <a href="{{route('product', $feature->id)}}"><img src="{{asset('images/' . $img[0])}}"
                                                                                alt=""></a>
                            <div class="text-center">
                                <h2>{{$feature->price}}$</h2>
                                <a href="{{route('product', $feature->id)}}"><p>{{$feature->title}}</p></a>
                                <p>{{$feature->category->title}}</p>
                                <a href="" class="btn btn-default"><i class="fa fa-shopping-cart"
                                                                      style="margin-right: 10px"></i>Add To Cart</a>
                            </div>
                        </div>
                        <div class=" add-block">
                            <ul>
                                <li class="col-xs-6"><a href=""><i class="fa fa-plus-square"></i> Add to wishlist</a>
                                </li>
                                <li class="col-xs-6 text-right"><a href=""><i class="fa fa-plus-square"></i> Add to
                                        compare</a></li>
                            </ul>
                        </div>
                    </div>
                @endforeach
            @endisset
        </div>


        <div class="main3">
            <h3><span>RECOMMENDED ITEMS</span></h3>
            <div id="id_carousel2" class="carousel slide">
                <!-- Slides -->
                <div class="carousel-inner">
                    @isset($recomentedItems)
                        @foreach ($recomentedItems as $key => $chunk)
                            @if($key == 0)
                                <div class="item active">
                                    @else
                                        <div class="item">
                                            @endif
                                            <div class="row">
                                                @foreach ($chunk as $recomentedItem)
                                                    @php $img = json_decode($recomentedItem->posters); @endphp
                                                    <div class="col-xs-12 col-sm-4">
                                                        <div class="text-center carousel_only_product">
                                                            <div class="carousel_product_image ">
                                                                <a href="{{route('product', $recomentedItem->id)}}"><img
                                                                            src="{{asset('images/' . $img[0])}}"
                                                                            alt="product1"></a>
                                                            </div>
                                                            <p>{{$recomentedItem->price}}$</p>
                                                            <a href="{{route('product', $recomentedItem->id)}}">{{$recomentedItem->title}}</a>
                                                            <form action="{{route('product', $feature->id)}}"
                                                                  method="get">
                                                                <button type="submit" class="btn btn-default">View
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endforeach
                                    @endisset
                                </div>
                                <!-- Controls -->
                                <a class="" href="#id_carousel2" data-slide="prev">
                                    <i class="fa fa-angle-left left_arrow_carousel2"></i>
                                </a>
                                <a class="" href="#id_carousel2" data-slide="next">
                                    <i class="fa fa-angle-right right_arrow_carousel2"></i>
                                </a>
                </div>


            </div>
        </div>
    </section>
@endsection


