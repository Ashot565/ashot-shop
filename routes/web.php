<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::namespace('admin')->prefix('eshop-admin')->middleware('auth','role')->group(function ()
{
    Route::get('/', function (){
        return view('home');
    });
    Route::resource('/category', 'CategoryController');
    Route::resource('/products', 'ProductController');
    Route::resource('/slider', 'SliderController');
    Route::post('/slider/update-order', 'SliderController@change_image_order');
});

Route::get('/', 'FrontController')->name('index');

Route::get('products', 'ProductListController@allProducts')->name('products');

Route::get('product/{id}', 'ProductListController@singleProduct')->name('product');

Route::get('category/{id}', 'ProductListController@categories')->name('category');

Route::post('comment', 'ProductListController@addComment')->name('comment');

Route::get('/search', 'ProductListController@search')->name('search');

Route::get('/chart', 'ChartController@index')->name('chart')->middleware('auth');

Route::post('/add-to-cart', 'ChartController@add')->name('add-to-cart');

Route::post('/hide-product', 'ChartController@hide_product')->name('hide_product');

Route::post('/avatar', 'ChartController@changeAvatar')->name('change_avatar');