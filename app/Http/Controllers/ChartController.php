<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChartController extends Controller
{
    public function index(){
        $products = Cart::where('user_id', Auth::user()->id)->get();

        if(view()->exists('chart')){
            return view('chart', compact('products'));
        }
    }

    public function add(Request $request){
        $user = Auth::user();
        $products = Cart::where('user_id', $user->id)->get();
        $input = $request->except('_token');
        $a = true;

        foreach ($products as $product) {
            if($product['web_id'] == $input['web_id']) {
                $input['quantity'] = $product['quantity'] + $input['quantity'];
                $input['total_price'] = $input['quantity'] * $input['price'];
                $input['buy'] = 0;
                $product->fill($input);
                $product->update();
                $a = false;
                break;
            }
        }

        if($a)
        {
            $input['total_price'] = $input['quantity'] * $input['price'];
            $input['buy'] = 0;
            $cart = new Cart;
            $cart->fill($input);
            $cart->save();
        }
        return redirect()->route('chart');
    }

    public  function  hide_product(Request $request){
        $prod = Cart::where('id', $request->id)->where('user_id',Auth::user()->id)->first();
        if (!empty($prod)){
            $prod ->delete();
            echo true;
        }else{
            echo false;
        }

    }

    public function  changeAvatar(Request $request)
    {
        if ($request->hasFile('user_avatar')){
            $avatar = $request->file('user_avatar');
            $fillname = rand(1,20) . $avatar->getClientOriginalName();
            $avatar->move(public_path() . '/images/' , $fillname);
            $user = Auth::user();
            $user->avatar = $fillname;
            $user->update();
            echo $fillname;
        }
    }
}
