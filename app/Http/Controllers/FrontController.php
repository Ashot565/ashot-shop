<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\ViewErrorBag;

class FrontController extends Controller
{
    function __invoke()
    {


        $categoryList = Category::all();

        $featuresList = Product::inRandomOrder()->limit(6)->get();

        $recomentedItems = Product::limit(12)->orderBy('quantity', 'desc')->get()->chunk(3);

        $sliders = Slider::orderBy('sort_id', 'ASC')->get();

        if (view()->exists('index')){
            return view('index', compact('categoryList', 'featuresList', 'recomentedItems', 'sliders'));
        }


    }
}
