<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Comment;


class ProductListController extends Controller
{
    public function allProducts(){
        $products = Product::paginate(3);
        $categoriesList = Category::all();
        return view('products', compact('products', 'categoriesList'));
    }

    public function categories($id){
        $products = Product::where('category_id',$id)->paginate(3);
        $categoryList = Category::all();
        return view('products', compact('products', 'categoryList'));
    }

    public function singleProduct($id){
        $product= Product::find($id);
        $categoryList = Category::all();
        $recomentedItems = Product::limit(12)->orderBy('quantity', 'desc')->get();
        $comments = Comment::where('product_id',$id)->orderBy('created_at', 'desc')->get();
        if (view()->exists('product')){
            return view('product', compact('product', 'categoryList', 'recomentedItems','comments'));
        }
    }
    function  addComment(Request $request)
    {
        $comment = new Comment;
        $comment->fill($request->all());
       $comment->save();
    }

    function search(Request $request)
    {
        $search = $request->search;
        if ($search == ''){
            return redirect()->back();
        }

        $products = Product::where('title', 'LIKE' , '%' . $search . '%')->paginate(3);
        $products->appends($request->only('search'));
        $categoryList = Category::all();
        if (view()->exists('products')){
            return view('products', compact('products', 'categoryList'));
        }

    }
}

























