<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Slider::orderBy('sort_id', 'ASC')->get();
        return view('admin.slider-list', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
           'images'=>'required' ,
            'images.*'=>'image|mimes:jpeg,jpg,png,gif|max:2048'
        ]);

        if ($valid->fails()) {
            return redirect()->route('slider.create')->withErrors($valid);
        }

        if ($request->hasFile('images')){
            foreach ($request->file('images') as $image) {
                $name = $image->getClientOriginalName();

                $image->move(public_path() . '/images/' , $name);

                $slaider = new Slider();
                $slaider->path = $name;

                if ($slaider->save()) {
                    $slaider->sort_id = $slaider->id;
                };
            }
        }return redirect('eshop-admin/slider')->with('status', 'your images has been successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_image_order(Request $request){
        $position = 1;
        foreach ($request->ids as $id){
            Slider::where('id', $id )->update(['sort_id' => $position]);
            $position++;
        }
        return 'OK';
    }
}
