<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products= Product::paginate(10);
        return view('admin.product',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.product-add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token','posters');
        $validator = Validator::make($input, [
           'title'=> 'required|max:255',
           'availability'=> 'required|max:255',
           'condition'=> 'required|max:255',
           'quantity'=> 'required|max:255',
           'price'=> 'required|max:255',
           'web_id'=> 'required|max:255',
           'category_id'=> 'required|max:255',
        ]);
        if ($validator->fails()){
            return redirect()
                ->route('products.create');
        }
        if ($request->hasFile('posters')){
            $files = $request->file('posters');
            $image=[];

            foreach ($files as $file){
                $file_name = rand(1,9999).$file->getClientOriginalName();
                array_push($image,$file_name);
                $file->move(public_path().'/images', $file_name);
            }
            $input['posters']= json_encode($image);
        }
        $product = new Product();
        $product->fill($input);
        $product->save();

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();

        return view('admin/product-edit', compact('product', "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $files = $request->except('_token', '_method');
        $product = Product::find($id);

        $validator = Validator::make($files, [

            'title'=> 'required|max:255',
            'availability'=> 'required|max:255',
            'condition'=> 'required|max:255',
            'quantity'=> 'required|max:255',
            'price'=> 'required|max:255',
            'web_id'=> 'required|max:255'
        ]);
        if ($validator->fails()){
            return redirect()->back();
        }
        $img=[];

        if (isset($files['posters'])){
            foreach ($files['posters'] as $file ){
                $imgName = rand(1,20). $file->getClientOriginalName();
                array_push($img,$imgName);
                $file->move(public_path().'/images', $imgName);
            }$files['posters']=json_encode($img);
        }
        $product->update($files);

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   $product = Product::find($id);
        $product-> delete();
        return redirect()->route('products.index');
    }
}
