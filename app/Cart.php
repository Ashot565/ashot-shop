<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable=[
        'user_id',
        'title',
        'poster',
        'web_id',
        'price',
        'total_price',
        'quantity',
        'buy'
    ];
}
