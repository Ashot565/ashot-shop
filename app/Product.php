<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable =[
        'title',
        'availability',
        'condition',
        'quantity',
        'price',
        'web_id',
        'posters',
        'category_id'
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }
}
